<?php 
    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $object = new Animal("shaun");
    
    echo "Animal name : $object->name <br>";
    echo "Animal legs : $object->legs <br>";
    echo "Blood type : $object->cold_blooded <br><br>";
    
    $kodok = new Frog("Buduk");
    echo "Animal name : $kodok->name <br>";
    echo "Animal legs : $kodok->legs <br>";
    echo "Blood type : $kodok->cold_blooded <br>";
    $kodok->jump();
    echo "<br><br>";
    
    $sungokong = new Ape("Kera sakti");
    echo "Animal name : $sungokong->name <br>";
    echo "Animal legs : $sungokong->legs <br>";
    echo "Blood type : $sungokong->cold_blooded <br>";
    $sungokong->yel();

    ?>